<?php

/**
 * User: Wisp X
 * Date: 2018/4/19
 * Time: 上午11:45
 * Link: http://gitee.com/wispx
 */

namespace wispx;

class Cookie
{
    /**
     * 实例
     * @var null
     */
    protected static $instance = null;

    /**
     * Cookie 前缀
     * @var string
     */
    protected $prefix = '';

    /**
     * Cookie 保存时间
     * @var int
     */
    protected $expire = 0;

    /**
     * Cookie 保存路径
     * @var string
     */
    protected $path = '/';

    /**
     * Cookie 有效域名
     * @var string
     */
    protected $domain = '';

    /**
     * 是否启用安全传输
     * @var bool
     */
    protected $secure = false;

    /**
     * httponly
     * @var bool
     */
    protected $httponly = false;

    /**
     * 是否使用setcookie
     * @var bool
     */
    protected $setcookie = true;

    public function __construct(array $options = [])
    {
        if (!empty($this->httponly)) {
            ini_set('session.cookie_httponly', 1);
        }

        if ($options) {
            foreach ($options as $key => $val) {
                isset($this->$key) && $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     * Instance
     * @param array $options
     * @return null|static
     */
    public static function instance($options = [])
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($options);
        }
        return self::$instance;
    }

    /**
     * 判断Cookie是否存在
     * @param   $name           Cookie 名称
     * @param   null $prefix Cookie 前缀
     * @return  bool
     */
    public function has($name, $prefix = null)
    {
        $prefix = !is_null($prefix) ? $prefix : $this->prefix;
        $name = $prefix . $name;
        return isset($_COOKIE[$name]);
    }

    /**
     * 设置Cookie
     * @param $name             Cookie 名称
     * @param string $value Cookie 值
     * @param string $prefix Cookie 前缀
     */
    public function set($name, $value = '', $prefix = '')
    {
        if (!empty($prefix)) $this->prefix = $prefix;

        $name = $this->prefix . $name;

        $expire = !empty($this->expire) ? $_SERVER['REQUEST_TIME'] + intval($this->expire) : 0;

        if (is_array($value)) {
            $value = json_encode($value);
        }

        if ($this->setcookie) {
            setcookie($name, $value, $expire, $this->path, $this->domain, $this->secure, $this->httponly);
        }

        $_COOKIE[$name] = $value;
    }

    /**
     * 获取Cookie数据
     * @param string $name Cookie 名称，为空获取所有
     * @param null $prefix Cookie 前缀
     * @return array|mixed|null
     */
    public function get($name = '', $prefix = null)
    {
        $name = !is_null($prefix) ? $prefix : $this->prefix . $name;

        if ('' == $name) {
            if ($prefix) {
                $value = [];
                foreach ($_COOKIE as $k => $val) {
                    if (0 === strpos($k, $prefix)) {
                        $value[$k] = $val;
                    }
                }
            } else {
                $value = $_COOKIE;
            }
        } elseif (isset($_COOKIE[$name])) {
            $value = $_COOKIE[$name];
            if (is_null(json_decode($value))) {
                $value = json_decode($value, true);
            }
        } else {
            $value = null;
        }
        return $value;
    }

    /**
     * 删除Cookie
     * @param $name         Cookie 名称
     * @param null $prefix Cookie 前缀
     */
    public function delete($name, $prefix = null)
    {
        $name = !is_null($prefix) ? $prefix : $this->prefix . $name;

        if ($this->setcookie) {
            setcookie($name, '', $_SERVER['REQUEST_TIME'] - 3600, $this->path, $this->domain, $this->secure, $this->httponly);
        }
        unset($_COOKIE[$name]);
    }

    /**
     * 清空Cookie
     * @param null $prefix Cookie 前缀
     */
    public function clear($prefix = null)
    {
        if (empty($_COOKIE)) {
            return;
        }

        $prefix = !is_null($prefix) ? $prefix : $this->prefix;
        if ($prefix) {
            foreach ($_COOKIE as $key => $val) {
                if (0 === strpos($key, $prefix)) {
                    if ($this->setcookie) {
                        setcookie($key, '', $_SERVER['REQUEST_TIME'] - 3600, $this->path, $this->domain, $this->secure, $this->httponly);
                    }
                    unset($_COOKIE[$key]);
                }
            }
        }
        return;
    }

    /**
     * 设置或获取Cookie前缀
     * @param string $prefix Cookie前缀
     * @return string
     */
    public function prefix($prefix = '')
    {
        if (empty($prefix)) {
            return $this->prefix;
        }
        $this->prefix = $prefix;
    }

    /**
     * 永久保存Cookie
     * @param $name             Cookie 名称
     * @param string $value Cookie 值
     * @param string $prefix Cookie 前缀
     */
    public function forever($name, $value = '', $prefix = '')
    {
        $this->expire = 315360000;

        $this->set($name, $value, $prefix);
    }
}