<?php

/**
 * User: Wisp X
 * Date: 2018/4/19
 * Time: 上午11:53
 * Link: http://gitee.com/wispx
 */

require __DIR__ . '/../vendor/autoload.php';

use wispx\Cookie;

// 实例化
$cookie = new Cookie();

// 可传入配置
/*
$cookie = new \wispx\Cookie([
    // Cookie前缀
    'prefix'    => '',
    // 过期时间
    'expire'    => 0,
    // 保存路径
    'path'      => '/',
    // 有效域名
    'domain'    => '',
    // 是否启用安全传输
    'secure'    => false,
    // 是否设置httponly
    'httponly'  => false,
    // 是否使用setCookie
    'setcookie' => true
]);
*/

// 判断Cookie是否已经被设置 [string $name, string $prefix = null]
$cookie->has('test');

// 设置Cookie [string $name, string $value, string $prefix = null]
$cookie->set('test', 123456);

// 获取Cookie [string $name, string $prefix = null]
$cookie->get('test');

// 永久保存Cookie [string $name, string $value = '', string $prefix = '']
$cookie->forever('test', 123456);

// 也可以单独设置Cookie前缀 [$prefix = '']
$cookie->prefix('w_');

// 删除Cookie [string $name, string $prefix = null]
$cookie->delete('test');

// 清空Cookie [$prefix = '']
$cookie->clear();

// 也可以这样使用
Cookie::instance()->has('test');